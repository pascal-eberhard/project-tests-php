# Project tests

## Description

To have only one repository for all my PHP release tests stuff.

Collect into one repository to avoid copies in every using repository.

### [Change log](resources/docs/changelog.md)

### License

The MIT License, see [License File](license.md).

## Components

The current goal is support for PHP Versions >= 7.1 .

### Code style

See [squizlabs](https://github.com/squizlabs/PHP_CodeSniffer).

```yaml
toCheck:
  file: "phpcs.phar"
  version: "3.5.2"
toFix:
  file: "phpcbf.phar"
  version: "3.5.2"
```

### Security

Symfony security checker.

```yaml
version: "6.0"
```

### Tests

PHPUnit Tests.

```yaml
version: "7.0.3"
```

## Installation

Via composer:

```bash
composer require pascal-eberhard/project-tests-php
```

## Some shell commands

```bash
# All checks
composer checks

# One at a time, see composer.json "scripts"
#composer [insert composer script label, without @ prefix]
```
