# Change log

## Version 1.0

```yaml
checks:
  bitBucket: "https://bitbucket.org/pascal-eberhard/project-tests-php/addon/pipelines/home#!/results/1"
  local:
    composerChecksPassed: true
    php: "7.2.1"
    operatingSystem:
      extry: "Git bash"
      name: "Windows 7"
repository:
  commit: "https://bitbucket.org/pascal-eberhard/project-tests-php/commits/25ae2fc26389c0d76dcaf18a4637beaf5c64b4d2"
```
